let cacheNKDA = "nkda_citizen";
this.addEventListener("install", event => {
	event.waitUntil(
		caches.open(cacheNKDA).then(cacheData => {
			cacheData.addAll([
				"/static/js/bundle.js",
				"/static/js/main.chunk.js",
				"/static/js/0.chunk.js",
				"/index.html",
				"/",
				"/login",
				"/forgotPassword",
			]);
		})
	);
});

this.addEventListener("fetch", event => {
	if (!navigator.onLine) {
		event.respondWith(
			caches.match(event.request).then(result => {
				return result;
			})
		);
	}
});
