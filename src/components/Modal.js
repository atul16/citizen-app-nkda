import React, { useState } from "react";
import { Modal, Button } from "antd";
import moment from "moment";
export default function ModalCont(props) {
	const [visible, setVisible] = useState(false);
	const {
		booking_id,
		citizen_id,
		amount,
		end_time,
		billed_at,
		parking_id,
		start_time,
		vehicle_reg_no,
		vehicle_make_model,
		vehicle_type,
	} = props.item;
	return (
		<div>
			<Button
				type='primary'
				shape='round'
				onClick={() => setVisible(true)}
				style={{ textTransform: "capitalize" }}
			>
				{props.name}
			</Button>
			<Modal
				centered
				visible={visible}
				onOk={() => setVisible(false)}
				onCancel={() => setVisible(false)}
				style={{ height: "auto" }}
				//bodyStyle={{ background: "#ffffffd6" }}
			>
				<img
					src={require("../images/logo.png")}
					style={{
						width: 50,
						height: 50,
						borderRadius: 3,
						border: "1px solid #eee",
					}}
				/>
				<p style={{ fontFamily: "open", color: "#999", fontSize: 20 }}>
					{props.name === "receipt" ? "Parking Receipt" : "Details"}
				</p>
				{props.name === "receipt" && (
					<div style={{ display: "flex", width: "100%", height: 70 }}>
						<div style={{ width: "40%" }}>
							<p id='details_title2'>
								<b className='title3'>Parking Id</b>&nbsp; {parking_id}
							</p>
							<p id='details_title2'>
								<b className='title3'>Customer Id</b>&nbsp; {citizen_id}
							</p>

							{end_time ? (
								<p id='details_title2'>
									<b className='title3'>Date</b> &nbsp;
									{moment(end_time).format("MMM Do YY")}
								</p>
							) : (
								"ongoing"
							)}
						</div>
						<div style={{ width: "60%", textAlign: "center" }}>
							<p id='details_title2'>
								<b className='title3'>Billed At</b>&nbsp; {billed_at}
							</p>
						</div>
					</div>
				)}

				{props.name === "receipt" && (
					<div
						style={{
							width: "100%",
							height: 1,
							background: "#ecebeb94",
							margin: 8,
						}}
					/>
				)}
				{props.name === "receipt" && (
					<p style={{ fontFamily: "openbold", fontSize: 17, color: "#222" }}>
						Billing desc
					</p>
				)}
				<div>
					{props.name !== "receipt" && (
						<p id='details_title2'>
							<b className='title3'>Parking Id</b>&nbsp; {parking_id}
						</p>
					)}
					<p id='details_title2'>
						<b className='title3'>Booking Id:</b>&nbsp; {booking_id}
					</p>
					<p id='details_title2' style={{ textTransform: "uppercase" }}>
						<b className='title3' style={{ textTransform: "initial" }}>
							Reg no:
						</b>
						&nbsp; {vehicle_reg_no}
					</p>
					<p id='details_title2'>
						<b className='title3'>Vehicle model</b>&nbsp; {vehicle_make_model}
					</p>
					<p id='details_title2'>
						<b className='title3'>Start Time</b> &nbsp;
						{moment(start_time).format("llll")}
					</p>
					{props.name === "receipt" && (
						<p id='details_title2'>
							<b className='title3'>End Time</b> &nbsp;
							{moment(end_time).format("llll")}
						</p>
					)}
					{props.name === "receipt" && (
						<p id='details_title2'>
							<b className='title3'>Amount</b>&nbsp;₹ {amount}
						</p>
					)}
				</div>
			</Modal>
		</div>
	);
}
