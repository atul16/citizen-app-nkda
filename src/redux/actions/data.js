import axios from "axios";

export const getVehicleList = data => dispatch => {
	axios
		.post("/get_vehicle_list", data)
		.then(res => {
			dispatch({ type: "VEHICLE_LIST", payload: res.data.data });
		})
		.catch(err => console.log(err));
};

export const availableBays = data => dispatch => {
	axios
		.post("/get_all_lot_details", data)
		.then(res => {
			dispatch({ type: "DETAILS", payload: res.data.data });
		})
		.catch(err => console.log(err));
};
export const getBookingList = data => dispatch => {
	axios
		.post("/get_booking_history", data)
		.then(res => {
			dispatch({ type: "BOOKING_LIST", payload: res.data.data });
		})
		.catch(err => console.log(err));
};
