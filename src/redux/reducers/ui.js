const INITIAL_STATE = {
	loading: false,
	error: "",
};

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case "LOADING":
			return { ...state, loading: action.payload };
		case "ERROR":
			return { ...state, error: action.payload };

		default:
			return state;
	}
}
