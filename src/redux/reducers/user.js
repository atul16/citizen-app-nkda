const INITIAL_STATE = {
	user: {},
	loggedIn: false,
};

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case "USER_INFO":
			return { ...state, user: action.payload };

		case "LOGGEDIN":
			return { ...state, loggedIn: action.payload };
		case "PICTURE":
			let arr = { ...state.user };

			arr.profile_picture = action.payload;
			return { ...state, user: arr };

		default:
			return state;
	}
}
