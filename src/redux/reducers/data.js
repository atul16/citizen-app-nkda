const INITIAL_STATE = {
	details: [],
	vehicle_list: [],
	booking_list: [],
	update_vehicle: [],
};

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case "DETAILS":
			return { ...state, details: action.payload };
		case "VEHICLE_LIST":
			return { ...state, vehicle_list: action.payload };
		case "BOOKING_LIST":
			return { ...state, booking_list: action.payload };
		case "UPDATE_VECHILE":
			return { ...state, update_vehicle: action.payload };

		default:
			return state;
	}
}
