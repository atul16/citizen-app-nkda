import { combineReducers } from "redux";
import data from "./data";
import user from "./user";
import ui from "./ui";

export default combineReducers({
	data,
	user,
	ui,
});
