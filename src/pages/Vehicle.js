import React from "react";
import { Col, Row } from "antd";
import { Form, Input, Button, Card, message, Popconfirm } from "antd";

import DriveEtaIcon from "@material-ui/icons/DriveEta";
import LocalTaxiOutlinedIcon from "@material-ui/icons/LocalTaxiOutlined";
import { Skeleton, Avatar } from "antd";
import { EditOutlined, EllipsisOutlined } from "@ant-design/icons";

import { Plus } from "react-bootstrap-icons";
import { Select } from "antd";
import { getVehicleList } from "../redux/actions";
import moment from "moment";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import { store } from "../redux/store";
import DirectionsBikeOutlinedIcon from "@material-ui/icons/DirectionsBikeOutlined";
const { Meta } = Card;

const { Option } = Select;

const useStyles = makeStyles(theme => ({
	box: {
		background: "#fff",
		padding: "18px",
		boxShadow: "0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
		marginTop: 30,
	},
}));

export default function Vehicle() {
	const [isLoading, setIsLoading] = React.useState(false);
	const [isToggle, setIsToggle] = React.useState(false);
	const [update, setUpdate] = React.useState(false);

	const { user } = useSelector(state => state.user);
	const { vehicle_list, update_vehicle } = useSelector(state => state.data);

	const dispatch = useDispatch();
	const classes = useStyles();

	const onFinish = values => {
		setIsLoading(true);
		const addVehicle = {
			user_id: user.user_id,
			vehicle_type: values.type,
			vehicle_make_model: values.vehicle_make + " " + values.vehicle_model,
			vehicle_reg_no: values.reg_no,
		};

		axios
			.post("/add_vehicle", addVehicle)
			.then(res => {
				setIsLoading(false);
				if (res.data.status === "success") {
					message.success("vehicle added successfully", 3);
					dispatch(getVehicleList({ user_id: user.user_id }));
				}
			})
			.catch(err => {
				setIsLoading(false);
			});
	};

	const onConfirm = vehicle_id => {
		const deleteVehicle = {
			user_id: user.user_id,
			vehicle_id,
		};

		axios
			.post("/delete_vehicle", deleteVehicle)
			.then(res => {
				console.log("after del", res.data);
				if (res.data.status === "success") {
					message.success("vehicle deleted successfully!");
					dispatch(getVehicleList({ user_id: user.user_id }));
				}
			})
			.catch(err => {});
	};

	const _setUpdate = data => {
		store.dispatch({ type: "UPDATE_VECHILE", payload: data });
		setUpdate(true);
	};

	const _onUpdate = values => {
		setIsLoading(true);
		const updateVehicle = {
			user_id: user.user_id,
			vehicle_type: values.type,
			vehicle_make_model: values.vehicle_make + " " + values.vehicle_model,
			vehicle_reg_no: values.reg_no,
			vehicle_id: update_vehicle.vehicle_id,
		};

		axios
			.post("/update_vehicle", updateVehicle)
			.then(res => {
				setIsLoading(false);
				if (res.data.status === "success") {
					dispatch(getVehicleList({ user_id: user.user_id }));

					setUpdate(false);
					message.success("vehicle details updated!!!");
				}
			})
			.catch(err => {
				setIsLoading(false);
			});
	};

	return (
		<>
			<Col
				xs={24}
				lg={24}
				style={{
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
					marginBottom: 20,
				}}
			>
				<div
					style={{
						width: 100,
						height: 40,
						background: "#fff",
						display: "flex",
						justifyContent: "center",
						fontWeight: 600,
						color: "#6b6b6b",
						alignItems: "center",
						borderRadius: 5,
						borderTopLeftRadius: 0,
						borderTopRightRadius: 0,
					}}
				>
					vehicle
				</div>
			</Col>
			{isToggle ? (
				<Col className='gutter-row' xs={22} lg={isToggle ? 4 : 5}>
					<div className={classes.box}>
						<p
							style={{
								fontWeight: 400,
								color: "#666",
								fontSize: 19,
								marginTop: 10,
							}}
						>
							Add Vehicle
						</p>
						<Form
							name='normal_login'
							className='login-form'
							initialValues={{
								remember: true,
							}}
							onFinish={onFinish}
						>
							<Form.Item
								name='vehicle_make'
								rules={[
									{
										required: true,
										message: "vehicle make required!",
									},
								]}
							>
								<Input placeholder='Vehicle Make' />
							</Form.Item>
							<Form.Item
								name='vehicle_model'
								rules={[
									{
										required: true,
										message: "vehicle model required",
									},
								]}
							>
								<Input placeholder='Vehicle Model' />
							</Form.Item>
							<Form.Item
								name='reg_no'
								rules={[
									{
										required: true,
										message: "reg no. required!",
									},
								]}
							>
								<Input placeholder='Registration no.' />
							</Form.Item>
							<Form.Item
								name='type'
								rules={[
									{
										required: true,
										message: "Please input vehicle type!",
									},
								]}
							>
								<Select
									style={{ textAlign: "left" }}
									placeholder='Select Vehicle Type'
								>
									<Option value='car'>car</Option>
									<Option value='bike'>bike</Option>
									<Option value='taxi'>taxi</Option>
									<Option value='other'>other</Option>
								</Select>
							</Form.Item>

							<div style={{ display: "flex", justifyContent: "space-around" }}>
								<Button
									type='dashed'
									htmlType='submit'
									className='login-form-button'
									style={{ width: "40%" }}
									onClick={() => setIsToggle(false)}
								>
									back
								</Button>
								<Button
									type='primary'
									htmlType='submit'
									className='login-form-button'
									style={{ width: "40%" }}
									loading={isLoading}
								>
									Add
								</Button>
							</div>
						</Form>
					</div>
				</Col>
			) : !update ? (
				vehicle_list.length > 0 ? (
					vehicle_list.map((data, i) => (
						<Col
							className='gutter-row'
							xs={22}
							lg={isToggle ? 4 : 5}
							style={{ margin: 5 }}
						>
							<div
								style={{
									width: "100%",
									height: 150,
									flexDirection: "column",
									background: "#fff",
									borderRadius: 4,
									boxShadow:
										"0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
									marginBottom: i === vehicle_list.length - 1 ? 100 : 0,
								}}
							>
								<div style={{ width: "100%", height: 100, display: "flex" }}>
									<div
										style={{
											width: "25%",
											height: 100,
											display: "flex",
											justifyContent: "center",
											alignItems: "center",
										}}
									>
										{data.vehicle_type === "car" ? (
											<DriveEtaIcon style={{ color: "#0b27c4" }} />
										) : data.vehicle_type === "bike" ? (
											<DirectionsBikeOutlinedIcon />
										) : data.vehicle_type === "taxi" ? (
											<LocalTaxiOutlinedIcon />
										) : (
											<b
												style={{
													fontWeight: 500,
													color: "#039af4",
													fontSize: 17,
												}}
											>
												o
											</b>
										)}
									</div>
									<div
										style={{
											width: "75%",
											height: 100,
											flexDirection: "column",
										}}
									>
										<div
											style={{
												textAlign: "left",
												padding: 2,
												paddingTop: 7,
												color: "#222",
												fontFamily: "openbold",
												textTransform: "capitalize",
											}}
										>
											{data.vehicle_make_model}
										</div>
										<div
											style={{
												textAlign: "left",
												paddingTop: 2,
												color: "#6b6b6b",
												fontFamily: "open",
												fontSize: 12,
												textTransform: "uppercase",
											}}
										>
											<b
												style={{
													color: "#222",
													fontWeight: 600,
													fontSize: 10,
												}}
											>
												Reg no.{" "}
											</b>
											&nbsp;
											{data.vehicle_reg_no}
										</div>
										<div
											style={{
												textAlign: "left",
												paddingTop: 2,
												color: "#6b6b6b",
												fontFamily: "open",
												fontSize: 12,
											}}
										>
											<b
												style={{
													color: "#222",
													fontWeight: 600,
													fontSize: 10,
												}}
											>
												Booking Time{" "}
											</b>{" "}
											&nbsp;
											{moment(data.updated_at).format("lll")}
										</div>
									</div>
								</div>
								<div
									style={{
										width: "100%",
										height: 50,
										display: "flex",
										justifyContent: "flex-end",
										alignItems: "center",
										border: "1px solid #eee",
										borderLeftColor: "transparent",
										borderRightColor: "transparent",
									}}
								>
									<div
										style={{
											width: "60%",
											height: 50,
											display: "flex",
											justifyContent: "space-around",
											alignItems: "center",
										}}
									>
										<Button
											type='primary'
											shape='round'
											icon={<EditOutlined />}
											size={80}
											onClick={() => _setUpdate(data)}
										>
											Edit
										</Button>
										<Popconfirm
											placement='topRight'
											title='Are you sure delete this vehicle?'
											onConfirm={() => onConfirm(data.vehicle_id)}
											// onCancel={cancel}
											okText='Yes'
											cancelText='No'
										>
											<Button shape='round' type='ghost'>
												Delete
											</Button>
										</Popconfirm>
									</div>
								</div>
							</div>
						</Col>
					))
				) : (
					<Card
						style={{ width: "100%", marginTop: 16 }}
						actions={[
							<EditOutlined key='edit' />,
							<EllipsisOutlined key='ellipsis' />,
						]}
					>
						<Skeleton
							loading={vehicle_list.length === 0 ? true : false}
							avatar
							active
						>
							<Meta
								avatar={
									<Avatar src='https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png' />
								}
								title='Card title'
								description='This is the description'
							/>
						</Skeleton>
					</Card>
				)
			) : (
				<Col className='gutter-row' xs={22} lg={isToggle ? 4 : 5}>
					<div className={classes.box}>
						<p
							style={{
								fontWeight: 400,
								color: "#666",
								fontSize: 19,
								marginTop: 10,
							}}
						>
							Update Vehicle Details
						</p>
						{update_vehicle && (
							<Form
								name='normal_login'
								className='login-form'
								initialValues={{
									remember: true,
								}}
								onFinish={_onUpdate}
							>
								<Form.Item
									initialValue={update_vehicle.vehicle_make_model.split(" ")[0]}
									name='vehicle_make'
									rules={[
										{
											required: true,
											message: "vehicle make required!",
										},
									]}
								>
									<Input placeholder='Vehicle Make' />
								</Form.Item>
								<Form.Item
									initialValue={update_vehicle.vehicle_make_model.split(" ")[1]}
									name='vehicle_model'
									rules={[
										{
											required: true,
											message: "vehicle model required",
										},
									]}
								>
									<Input placeholder='Vehicle Model' />
								</Form.Item>
								<Form.Item
									initialValue={update_vehicle.vehicle_reg_no}
									name='reg_no'
									rules={[
										{
											required: true,
											message: "reg no. required!",
										},
									]}
								>
									<Input placeholder='Registration no.' />
								</Form.Item>
								<Form.Item
									initialValue={update_vehicle.vehicle_type}
									name='type'
									rules={[
										{
											required: true,
											message: "Please input vehicle type!",
										},
									]}
								>
									<Select
										style={{ textAlign: "left" }}
										placeholder='Select Vehicle Type'
									>
										<Option value='car'>car</Option>
										<Option value='bike'>bike</Option>
										<Option value='taxi'>taxi</Option>
										<Option value='other'>other</Option>
									</Select>
								</Form.Item>

								<div
									style={{ display: "flex", justifyContent: "space-around" }}
								>
									<Button
										type='dashed'
										htmlType='submit'
										className='login-form-button'
										style={{ width: "40%" }}
										onClick={() => setUpdate(false)}
									>
										back
									</Button>
									<Button
										type='primary'
										htmlType='submit'
										className='login-form-button'
										style={{ width: "40%" }}
										loading={isLoading}
									>
										Update
									</Button>
								</div>
							</Form>
						)}
					</div>
				</Col>
			)}

			{!isToggle && (
				<div
					style={{
						width: 40,
						height: 40,
						borderRadius: 100,
						background: "#fff",
						position: "fixed",
						bottom: 12,
						right: 12,
						display: "flex",
						justifyContent: "center",
						alignItems: "center",
						cursor: "pointer",
						boxShadow:
							"0 3px 6px -4px rgba(0, 0, 0, 0.12), 0 6px 16px 0 rgba(0, 0, 0, 0.08), 0 9px 28px 8px rgba(0, 0, 0, 0.05)",
					}}
					onClick={() => setIsToggle(!isToggle)}
				>
					<Plus size={22} />
				</div>
			)}
		</>
	);
}
