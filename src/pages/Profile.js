import React, { useState } from "react";
import { Col } from "antd";
import { Form, Input, Button, Avatar } from "antd";

import { useSelector } from "react-redux";
import axios from "axios";
import { store } from "../redux/store";
export default function Signup() {
	const [pic, setPic] = useState("");
	const [isLoading, setIsLoading] = React.useState(false);

	const { user } = useSelector(state => state.user);

	React.useEffect(() => {
		setPic(user.profile_picture);
	}, [user]);

	const style = {
		background: "#fff",
		padding: "18px",
		boxShadow: "0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
		marginTop: 30,
	};
	const onFinish = values => {
		setIsLoading(true);

		const userData = {
			user_id: user.user_id,
			name: values.name,
			email: values.email,
			password: values.password,
			photo_url: pic,
		};

		console.log(userData, "Received values of form: ", values);

		axios
			.post("/update_profile", userData)
			.then(res => {
				console.log(res.data);
				store.dispatch({ type: "PICTURE", payload: pic });
				setIsLoading(false);
			})
			.catch(err => {
				console.log(err);
				setIsLoading(false);
			});
	};

	const _handleImageChange = e => {
		e.preventDefault();

		let reader = new FileReader();
		let file = e.target.files[0];

		reader.onloadend = () => {
			setPic(reader.result);
		};

		reader.readAsDataURL(file);
	};

	return (
		<>
			<Col
				xs={24}
				lg={24}
				style={{
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
					marginBottom: 0,
				}}
			>
				<div
					style={{
						width: 100,
						height: 40,
						background: "#fff",
						display: "flex",
						justifyContent: "center",
						fontWeight: 600,
						color: "#6b6b6b",
						alignItems: "center",
						borderRadius: 5,
						borderTopLeftRadius: 0,
						borderTopRightRadius: 0,
					}}
				>
					Profile
				</div>
			</Col>
			<Col className='gutter-row' xs={22} lg={5} style={{ marginBottom: 100 }}>
				<div style={style}>
					<p
						style={{
							fontWeight: 400,
							color: "#666",
							fontSize: 19,
							marginTop: 10,
						}}
					>
						Update Profile
					</p>
					<input
						hidden
						id='file-input'
						type='file'
						onChange={e => _handleImageChange(e)}
						style={{ opacity: 0, position: "absolute" }}
					/>

					{pic !== "" ? (
						<label for='file-input'>
							<Avatar src={pic} size={80} />
						</label>
					) : (
						<label for='file-input'>
							<Avatar
								style={{
									backgroundColor: "#cbc5c5",
									verticalAlign: "middle",
									fontSize: 50,
									textTransform: "capitalize",
								}}
								size={80}
							>
								{user.name.charAt(0)}
							</Avatar>
						</label>
					)}
					<Form
						name='normal_login'
						className='login-form'
						initialValues={{
							remember: true,
						}}
						onFinish={onFinish}
						layout='vertical'
					>
						<Form.Item
							initialValue={user.name}
							name='name'
							rules={[
								{
									required: true,
									message: "name required!",
								},
							]}
							label='Name'
						>
							<Input placeholder='username' />
						</Form.Item>
						<Form.Item
							initialValue={user.email}
							name='email'
							rules={[
								{
									required: true,
									message: "email required",
								},
								{
									pattern: /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/,
									message: "Please enter a valid email ID",
								},
							]}
							label='Email'
						>
							<Input placeholder='email' />
						</Form.Item>
						<Form.Item
							initialValue={user.phone_no}
							name='phone'
							rules={[
								{
									// required: true,
									message: "phone no. required!",
								},
							]}
							label='Phone'
						>
							<Input placeholder='Phone no.' disabled />
						</Form.Item>
						<Form.Item
							name='password'
							rules={[
								{
									required: true,
									message: "Please input your Password!",
								},
								{
									min: 8,
								},
							]}
							label='Password'
						>
							<Input placeholder='Password' />
						</Form.Item>

						<div style={{ display: "flex", justifyContent: "space-around" }}>
							<Button
								type='primary'
								htmlType='submit'
								className='login-form-button'
								loading={isLoading}
							>
								Update
							</Button>
						</div>
					</Form>
				</div>
			</Col>
		</>
	);
}
