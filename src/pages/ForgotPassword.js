import React, { useState } from "react";
import { Row, Col, Divider } from "antd";
import { Layout } from "antd";
import { Form, Input, Button, Checkbox, Card, Alert } from "antd";
import {
	UserOutlined,
	LockOutlined,
	PhoneOutlined,
	MailOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { store } from "../redux/store";
import { Link } from "react-router-dom";

const { Content } = Layout;

export default function ForgotPassword(props) {
	const dispatch = useDispatch();
	const [toggleOtp, setToggleOtp] = useState(false);
	const [val, setVal] = useState({
		name: "",
		phone: "",
		email: "",
		password: "",
		photo_url: "",
	});
	const [isLoading, setIsLoading] = useState(false);
	const [isLoading2, setIsLoading2] = useState(false);

	const [error, setError] = useState("");
	const [success, setSuccess] = useState("");

	const style = {
		background: "#fff",
		padding: "18px",
		boxShadow: "0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
		borderRadius: 2,
	};
	const onFinish = values => {
		console.log("Received values of form: ", values);

		setVal({
			...val,

			phone: values.phone,
		});

		sendOtp(values.phone);
	};

	const sendOtp = phone => {
		setIsLoading2(true);
		axios
			.post("/forgot_password", { phone: phone })
			.then(res => {
				setIsLoading2(false);
				console.log(res.data);
				if (res.data.status === "success") {
					setToggleOtp(true);
				}
			})
			.catch(err => {
				setIsLoading2(false);
				setError("something wrong");
				setTimeout(() => {
					setError("");
				}, 4000);
			});
	};

	const changePassword = values => {
		setIsLoading(true);
		axios
			.post("/change_password", {
				phone: val.phone,
				otp: values.otp,
				new_password: values.new_password,
			})
			.then(res => {
				setIsLoading(false);
				console.log(res.data);
				if (res.data.status === "success") {
					setSuccess("Password Successfully changed!");
					setTimeout(() => {
						setSuccess("");
						props.history.push("/login");
					}, 2000);
				} else {
					setError("wrong otp");
					setTimeout(() => {
						setError("");
					}, 4000);
				}
			})
			.catch(err => {
				setIsLoading(false);
				console.log(err);
				setError("wrong otp");
				setTimeout(() => {
					setError("");
				}, 4000);
			});
	};

	return (
		<>
			<Layout className='layout' style={{ height: "100vh" }}>
				<img
					src={require("../images/images.jpg")}
					style={{
						width: "100%",
						height: "100%",
						position: "fixed",
						objectFit: "cover",
					}}
				/>
				<Content style={{ background: "#f7f7f7" }}>
					<Row justify='center' style={{ height: "100vh" }}>
						<Col
							className='gutter-row'
							xs={18}
							lg={5}
							style={{
								justifyContent: "center",
								alignItems: "center",
								display: "flex",
							}}
						>
							{" "}
							<div style={style}>
								<img
									src={require("../images/logo.png")}
									style={{
										width: 80,
										height: 80,

										opacity: 0.7,
										objectFit: "cover",
										//boxShadow: "5px 5px 25px #c9c9c9",
										border: "1px solid #eee",
										borderRadius: 3,
									}}
								/>
								<p
									style={{
										fontFamily: "openbold",
										color: "#666",
										fontSize: 10,
										marginTop: 10,
									}}
								>
									NKDA Smart Parking Citizen
								</p>
								{error !== "" && (
									<Alert message={error} banner style={{ marginBottom: 12 }} />
								)}
								{success !== "" && (
									<Alert
										message={success}
										banner
										style={{ marginBottom: 12 }}
										type='success'
									/>
								)}
								{!toggleOtp ? (
									<Form
										name='normal_login'
										className='login-form'
										initialValues={{
											remember: true,
										}}
										onFinish={onFinish}
									>
										<p style={{ fontFamily: "open", fontSize: 15 }}>
											Change Password Request
										</p>
										<Form.Item
											name='phone'
											rules={[
												{
													required: true,
													message: "Please input your Password!",
												},
											]}
										>
											<Input
												placeholder='Enter Phone no.'
												prefix={
													<PhoneOutlined className='site-form-item-icon' />
												}
											/>
										</Form.Item>

										<Form.Item>
											<Button
												type='primary'
												htmlType='submit'
												className='login-form-button'
												style={{ width: "100%" }}
												loading={isLoading2}
											>
												Send Request
											</Button>
										</Form.Item>
										<Divider />

										<Link to='/login'>
											<p style={{ fontFamily: "openbold", color: "#6b6b6b" }}>
												Back to Login
											</p>
										</Link>
									</Form>
								) : (
									<Form
										name='normal_login'
										className='login-form'
										initialValues={{
											remember: true,
										}}
										onFinish={changePassword}
									>
										<p
											style={{
												color: "#6b6b6b",
												fontFamily: "open",
												fontSize: 13,
											}}
										>
											Otp has been sent to{" "}
											<b style={{ color: "#222" }}>{val.phone}</b>
										</p>
										<p style={{ fontSize: 20 }}>Otp Verification</p>

										<Form.Item
											name='otp'
											rules={[
												{
													required: true,
													message: "Please enter Otp!",
												},
											]}
										>
											<Input
												// prefix={<UserOutlined className='site-form-item-icon' />}
												placeholder='Enter Otp'
											/>
										</Form.Item>
										<Form.Item
											name='phone'
											rules={[
												{
													required: true,
													message: "Please enter phone no.",
												},
											]}
											initialValue={val.phone}
										>
											<Input
												prefix={
													<PhoneOutlined className='site-form-item-icon' />
												}
												placeholder='Phone no.'
												disabled
											/>
										</Form.Item>
										<Form.Item
											name='new_password'
											rules={[
												{
													required: true,
													message: "Please enter new password!",
												},
											]}
										>
											<Input
												// prefix={<UserOutlined className='site-form-item-icon' />}
												placeholder='Enter New Password'
											/>
										</Form.Item>
										<Form.Item>
											<Button
												type='primary'
												htmlType='submit'
												className='login-form-button'
												style={{ width: "100%" }}
												loading={isLoading}
											>
												Change Password
											</Button>
										</Form.Item>
										<Divider />

										<Button
											loading={isLoading2}
											style={{
												fontWeight: "openbold",
												color: "#878787",
												cursor: "pointer",
												background: "#fff",
											}}
											onClick={() => sendOtp(val.phone)}
										>
											Resend OTP
										</Button>
										<Link to='/login'>
											<p
												style={{
													fontFamily: "openbold",
													color: "#6b6b6b",
													marginTop: 20,
												}}
											>
												Back to Login
											</p>
										</Link>
									</Form>
								)}
							</div>
						</Col>
					</Row>
				</Content>
			</Layout>
		</>
	);
}
