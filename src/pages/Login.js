import React, { useState } from "react";
import { Row, Col, Divider, Alert } from "antd";
import { Layout } from "antd";
import { Form, Input, Button, Checkbox, Card } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import Signup from "./Signup";
import { Link } from "react-router-dom";
import axios from "axios";
import { store } from "../redux/store";
const { Content } = Layout;

const LogIn = () => {
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState("");

	const style = {
		background: "#fff",
		padding: "18px",
	};

	const onFinish = values => {
		console.log("Received values of form: ", values);

		setIsLoading(true);
		axios
			.post("/login", values)
			.then(res => {
				console.log(res.data);

				setIsLoading(false);
				if (res.data.status === "error") {
					setError(res.data.data);
					setTimeout(() => {
						setError("");
					}, 4000);
				} else {
					store.dispatch({ type: "USER_INFO", payload: res.data.data });
					store.dispatch({ type: "LOGGEDIN", payload: true });
				}
			})
			.catch(err => {
				setIsLoading(false);
				console.log(err.response);
				setError("something wrong");
				setTimeout(() => {
					setError("");
				}, 4000);
			});
	};

	return (
		<div style={style}>
			<img
				src={require("../images/logo.png")}
				style={{
					width: 80,
					height: 80,
					opacity: 0.7,
					objectFit: "cover",
					// boxShadow: "5px 5px 25px #c9c9c9",
					border: "1px solid #eee",
					borderRadius: 3,
				}}
			/>
			<p
				style={{
					fontWeight: 400,
					color: "#666",
					fontSize: 12,
					marginTop: 10,
					fontFamily: "openbold",
				}}
			>
				NKDA Smart Parking Citizen
			</p>

			<p
				style={{
					fontWeight: 400,
					color: "#222",
					fontSize: 17,
					marginTop: 10,
					fontFamily: "open",
				}}
			>
				Login
			</p>
			{error !== "" && (
				<Alert message={error} banner style={{ marginBottom: 12 }} />
			)}
			<Form
				name='normal_login'
				className='login-form'
				initialValues={{
					remember: true,
				}}
				onFinish={onFinish}
			>
				<Form.Item
					name='username'
					rules={[
						{
							required: true,
							message: "Please input your Username!",
						},
					]}
				>
					<Input
						prefix={<UserOutlined className='site-form-item-icon' />}
						placeholder='Email or Phone no.'
					/>
				</Form.Item>
				<Form.Item
					name='password'
					rules={[
						{
							required: true,
							message: "Please input your Password!",
						},
					]}
				>
					<Input.Password
						placeholder='Password'
						prefix={<LockOutlined className='site-form-item-icon' />}
					/>
				</Form.Item>
				<Form.Item>
					<Form.Item name='remember' valuePropName='checked' noStyle>
						<Checkbox>Remember me</Checkbox>
					</Form.Item>
				</Form.Item>

				<Form.Item>
					<Button
						type='primary'
						htmlType='submit'
						className='login-form-button'
						style={{ width: "100%" }}
						loading={isLoading}
					>
						Log In
					</Button>
				</Form.Item>
				<Divider />
				<Link
					className='login-form-forgot'
					to='/forgotPassword'
					style={{ fontFamily: "open" }}
				>
					Forgot password?
				</Link>
			</Form>
		</div>
	);
};

export default function LoginSignUpContainer() {
	const tabListNoTitle = [
		{
			key: "Login",
			tab: "Login",
		},
		{
			key: "Signup",
			tab: "Signup",
		},
	];

	const contentListNoTitle = {
		Login: <LogIn />,
		Signup: <Signup />,
	};
	const [noTitleKey, setKey] = React.useState("Login");

	const onTabChange = (key, type) => {
		console.log(key, type);
		setKey(key);
	};

	return (
		<>
			<Layout style={{ height: "100vh" }}>
				<img
					src={require("../images/images.jpg")}
					style={{
						width: "100%",
						height: "100%",
						position: "fixed",
						objectFit: "cover",
					}}
				/>

				<Content style={{ background: "#fff", padding: 5 }}>
					<Row justify='center' style={{ height: "100vh" }}>
						<Col
							className='gutter-row'
							xs={18}
							lg={5}
							style={{
								justifyContent: "center",
								alignItems: "center",
								display: "flex",
							}}
						>
							<Card
								style={{
									width: "100%",
									boxShadow:
										"0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
								}}
								tabList={tabListNoTitle}
								activeTabKey={noTitleKey}
								onTabChange={key => {
									onTabChange(key, "noTitleKey");
								}}
							>
								{contentListNoTitle[noTitleKey]}
							</Card>
						</Col>
					</Row>
				</Content>
			</Layout>
		</>
	);
}
