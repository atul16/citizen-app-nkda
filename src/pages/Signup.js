import React, { useState } from "react";
import { Divider } from "antd";
import { Layout } from "antd";
import { Form, Input, Button, Alert } from "antd";
import {
	UserOutlined,
	LockOutlined,
	PhoneOutlined,
	MailOutlined,
} from "@ant-design/icons";
import { useDispatch } from "react-redux";
import axios from "axios";
import { store } from "../redux/store";

export default function Signup() {
	const dispatch = useDispatch();
	const [toggleOtp, setToggleOtp] = useState(false);
	const [val, setVal] = useState({
		name: "",
		phone: "",
		email: "",
		password: "",
		photo_url: "",
	});
	const [isLoading, setIsLoading] = useState(false);
	const [isLoading2, setIsLoading2] = useState(false);

	const [error, setError] = useState("");
	const [success, setSuccess] = useState("");

	// const { loading, error } = useSelector(state => state.ui);
	const style = {
		background: "#fff",
		padding: "18px",
	};
	const onFinish = values => {
		console.log("Received values of form: ", values);

		setVal({
			...val,
			name: values.name,
			phone: values.phone,
			email: values.email,
			password: values.password,
			photo_url: "",
		});

		sendOtp(values.phone);
	};

	const sendOtp = phone => {
		setIsLoading2(true);
		axios
			.post("/generate_registration_otp", { phone: phone })
			.then(res => {
				setIsLoading2(false);
				console.log(res.data);
				if (res.data.status === "success") {
					setToggleOtp(true);
				}
			})
			.catch(err => {
				setIsLoading2(false);
				setError("something wrong");
				setTimeout(() => {
					setError("");
				}, 4000);
			});
	};

	const verifyOtp = values => {
		setIsLoading(true);
		axios
			.post("/verify_registration_otp", { phone: val.phone, otp: values.otp })
			.then(res => {
				if (res.data.status === "success" && res.data.verified === 1) {
					setSuccess("otp verified");
					setTimeout(() => {
						setSuccess("");
					}, 4000);
					onSubmit();
				}
			})
			.catch(err => {
				setIsLoading(false);

				if (err.response.data.verified === 0) {
					setError("wrong otp");
					setTimeout(() => {
						setError("");
					}, 4000);
				}
			});
	};

	const onSubmit = () => {
		axios
			.post("/signup", val)
			.then(res => {
				console.log(res.data);
				store.dispatch({ type: "USER_INFO", payload: res.data.data });
				store.dispatch({ type: "LOGGEDIN", payload: true });
				setIsLoading(false);
			})
			.catch(err => {
				setIsLoading(false);
				setToggleOtp(false);
				setSuccess("");

				setError(err.response.data.message);
				setTimeout(() => {
					setError("");
				}, 5000);
			});
	};

	return (
		<>
			<div style={style}>
				<img
					src={require("../images/logo.png")}
					style={{
						width: 80,
						height: 80,

						opacity: 0.7,
						objectFit: "cover",
						//boxShadow: "5px 5px 25px #c9c9c9",
						border: "1px solid #eee",
						borderRadius: 3,
					}}
				/>
				<p
					style={{
						fontWeight: 400,
						color: "#666",
						fontSize: 12,
						marginTop: 10,
						fontFamily: "openbold",
					}}
				>
					NKDA Smart Parking Citizen
				</p>

				<p
					style={{
						fontWeight: 400,
						color: "#222",
						fontSize: 17,
						marginTop: 10,
						fontFamily: "open",
					}}
				>
					Sign Up
				</p>
				{error !== "" && (
					<Alert message={error} banner style={{ marginBottom: 12 }} />
				)}
				{success !== "" && (
					<Alert
						message={success}
						banner
						style={{ marginBottom: 12 }}
						type='success'
					/>
				)}
				{!toggleOtp ? (
					<Form
						name='normal_login'
						className='login-form'
						initialValues={{
							remember: true,
						}}
						onFinish={onFinish}
					>
						<Form.Item
							name='name'
							rules={[
								{
									required: true,
									message: "Please input your Username!",
								},
							]}
						>
							<Input
								prefix={<UserOutlined className='site-form-item-icon' />}
								placeholder='Username'
							/>
						</Form.Item>
						<Form.Item
							name='email'
							rules={[
								{
									required: true,
									message: "Please input your email!",
								},
								{
									pattern: /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/,
									message: "Please enter a valid email ID",
								},
							]}
						>
							<Input
								placeholder='Email'
								prefix={<MailOutlined className='site-form-item-icon' />}
							/>
						</Form.Item>
						<Form.Item
							name='phone'
							rules={[
								{
									required: true,
									message: "Please input your Password!",
								},
							]}
						>
							<Input
								placeholder='Phone'
								prefix={<PhoneOutlined className='site-form-item-icon' />}
							/>
						</Form.Item>
						<Form.Item
							name='password'
							rules={[
								{
									required: true,
									message: "Please input your Password!",
								},
								{
									min: 8,
								},
							]}
						>
							<Input.Password
								placeholder='Password'
								prefix={<LockOutlined className='site-form-item-icon' />}
							/>
						</Form.Item>

						<Form.Item>
							<Button
								type='primary'
								htmlType='submit'
								className='login-form-button'
								style={{ width: "100%" }}
								loading={isLoading2}
							>
								Sign Up
							</Button>
						</Form.Item>
					</Form>
				) : (
					<Form
						name='normal_login'
						className='login-form'
						initialValues={{
							remember: true,
						}}
						onFinish={verifyOtp}
					>
						<p
							style={{
								color: "#6b6b6b",
								fontFamily: "open",
								fontSize: 13,
							}}
						>
							Otp has been sent to <b style={{ color: "#222" }}>{val.phone}</b>
						</p>
						<p style={{ fontSize: 20 }}>Otp Verification</p>
						<Form.Item
							name='otp'
							rules={[
								{
									required: true,
									message: "Please enter Otp!",
								},
							]}
						>
							<Input
								// prefix={<UserOutlined className='site-form-item-icon' />}
								placeholder='Enter Otp'
							/>
						</Form.Item>

						<Form.Item>
							<Button
								type='primary'
								htmlType='submit'
								className='login-form-button'
								style={{ width: "100%" }}
								loading={isLoading}
							>
								Verify Otp
							</Button>
						</Form.Item>
						<Divider />

						<Button
							loading={isLoading2}
							style={{
								fontWeight: "openbold",
								color: "#878787",
								cursor: "pointer",
								background: "#fff",
							}}
							onClick={() => sendOtp(val.phone)}
						>
							Resend OTP
						</Button>
					</Form>
				)}
			</div>
		</>
	);
}
