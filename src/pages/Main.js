import React, { useState } from "react";
import { Layout, Menu, Drawer, Divider, Row, Avatar } from "antd";
import { List } from "react-bootstrap-icons";
import Vehicle from "./Vehicle";
import Booking from "./BookingHistory";
import Profile from "./Profile";
import Home from "./Home";
import { logout } from "../redux/actions";
import { useSelector, useDispatch } from "react-redux";
import { Offline, Online } from "react-detect-offline";

import {
	HomeOutlined,
	BookOutlined,
	CarOutlined,
	UserOutlined,
	LogoutOutlined,
} from "@ant-design/icons";
const { Header, Content } = Layout;

export default function Main() {
	const dispatch = useDispatch();
	const [visible, setVisible] = useState(false);
	const { user } = useSelector(state => state.user);
	const onClose = () => {
		setVisible(false);
	};
	document.body.style.backgroundColor = "#f7f7f7";

	const [values, setValues] = React.useState({
		comp: "Home",
	});
	const changeComp = comp => {
		setValues({ comp });
		onClose();
	};
	const comp = comp => {
		switch (comp) {
			case "Home":
				document.title = comp + " - NKDA Citizen";
				return <Home />;
			case "Booking":
				document.title = comp + " - NKDA Citizen";
				return <Booking />;
			case "Vehicle":
				document.title = comp + " - NKDA Citizen";
				return <Vehicle />;
			case "Profile":
				document.title = comp + " - NKDA Citizen";
				return <Profile />;

			default:
				document.title = "Home - NKDA Citizen";
				return <Home />;
		}
	};

	return (
		<>
			<Layout className='layout'>
				<Header
					style={{
						padding: 0,
						boxShadow:
							"0 0 4px 0 rgba(17,22,26,.16), 0 2px 4px 0 rgba(17,22,26,.08), 0 4px 8px 0 rgba(17,22,26,.08)",
					}}
				>
					<div className='logo' />
					<Menu theme='dark' mode='horizontal' defaultSelectedKeys={["2"]}>
						<Menu.Item onClick={() => setVisible(true)}>
							<List size={25} style={{ color: "#222", marginTop: 20 }} />{" "}
						</Menu.Item>
						<Menu.Item
							key='1'
							style={{
								color: "#222",
								verticalAlign: "top",
								fontFamily: "openbold",
								textShadow:
									"0 1px 3px rgba(0,0,0,.4), 0 0 30px rgba(0,0,0,.075)",
								fontSize: 15,
							}}
						>
							<img
								src={require("../images/logo.png")}
								style={{
									width: 30,
									height: 30,
									border: "1px solid #eee",
									margin: 5,
									marginLeft: -15,
								}}
							/>
							NKDA Smart Parking Citizen
						</Menu.Item>
					</Menu>
				</Header>
				<Content style={{ background: "#f7f7f7" }}>
					<Offline>
						<div
							style={{
								width: "100%",
								textAlign: "center",
								background: "#e0d6d5",
								color: "red",
								padding: 2,
								fontFamily: "open",
								letterSpacing: 1,
							}}
						>
							offline
						</div>
					</Offline>
					<Row justify='center'>{values.comp && comp(values.comp)}</Row>
				</Content>
			</Layout>
			<Drawer
				title={
					<div>
						{user.profile_picture ? (
							<Avatar src={user.profile_picture} style={{}} />
						) : (
							<Avatar
								style={{
									background: " #222",
									textTransform: "capitalize",
									fontFamily: "open",
									fontSize: 20,
								}}
							>
								{user.name.charAt(0)}
							</Avatar>
						)}{" "}
						&nbsp;&nbsp;
						<b style={{ textTransform: "capitalize", fontFamily: "open" }}>
							{user.name}
						</b>
					</div>
				}
				placement={"left"}
				closable={false}
				onClose={onClose}
				visible={visible}
				key={"left"}
			>
				<div
					className='list'
					onClick={() => changeComp("Home")}
					style={{ color: values.comp === "Home" ? "#0b27c4" : "#6b6b6b" }}
				>
					<HomeOutlined style={{ padding: 7 }} />
					Home
				</div>
				<div
					className='list'
					onClick={() => changeComp("Booking")}
					style={{ color: values.comp === "Booking" ? "#0b27c4" : "#6b6b6b" }}
				>
					<BookOutlined style={{ padding: 7 }} />
					Booking History
				</div>
				<div
					className='list'
					onClick={() => changeComp("Vehicle")}
					style={{ color: values.comp === "Vehicle" ? "#0b27c4" : "#6b6b6b" }}
				>
					<CarOutlined style={{ padding: 7 }} />
					Vehicle
				</div>
				<Divider style={{ margin: 4, border: "none" }} />
				<div
					className='list'
					onClick={() => changeComp("Profile")}
					style={{ color: values.comp === "Profile" ? "#0b27c4" : "#6b6b6b" }}
				>
					<UserOutlined style={{ padding: 7 }} />
					Profile
				</div>
				<div
					className='list'
					style={{ color: "#6b6b6b" }}
					onClick={() => dispatch(logout())}
				>
					<LogoutOutlined style={{ padding: 7 }} />
					Log Out
				</div>
			</Drawer>
		</>
	);
}
