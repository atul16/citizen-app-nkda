import React from "react";
import { Row, Col } from "antd";
import { Button, Card } from "antd";

import DriveEtaIcon from "@material-ui/icons/DriveEta";
import LocalTaxiOutlinedIcon from "@material-ui/icons/LocalTaxiOutlined";
import { Skeleton, Avatar } from "antd";
import { EditOutlined, EllipsisOutlined } from "@ant-design/icons";
import { CreditCardFill } from "react-bootstrap-icons";

import moment from "moment";
import { useSelector, useDispatch } from "react-redux";
import DirectionsBikeOutlinedIcon from "@material-ui/icons/DirectionsBikeOutlined";
import { Tabs } from "antd";
import Modal from "../components/Modal";

const { TabPane } = Tabs;
const { Meta } = Card;

export default function Signup() {
	// const [isLoading, setIsLoading] = React.useState(false);
	// const [isToggle, setIsToggle] = React.useState(false);

	// const { user } = useSelector(state => state.user);
	const { booking_list } = useSelector(state => state.data);

	// const dispatch = useDispatch();
	// const style = {
	// 	background: "#fff",
	// 	padding: "18px",
	// 	boxShadow: "0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
	// 	marginTop: 30,
	// };

	const pastLength = booking_list.filter(data => data.end_time !== null).length;

	return (
		<>
			<Col
				xs={24}
				lg={24}
				style={{
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
					marginBottom: 20,
				}}
			>
				<div
					style={{
						width: 170,
						height: 40,
						background: "#fff",
						display: "flex",
						justifyContent: "center",
						fontWeight: 600,
						color: "#6b6b6b",
						alignItems: "center",
						borderRadius: 5,
						borderTopLeftRadius: 0,
						borderTopRightRadius: 0,
					}}
				>
					Booking History
				</div>
			</Col>

			<Tabs defaultActiveKey='1' centered style={{ width: "100%" }}>
				<TabPane tab='Current' key='1' style={{ width: "100%" }}>
					<Row justify='center'>
						{booking_list.length > 0 ? (
							booking_list.map((data, i) => (
								<Col
									className='gutter-row'
									xs={22}
									lg={5}
									style={{ margin: 5 }}
								>
									<div
										style={{
											width: "100%",
											height: 180,
											flexDirection: "column",

											background: "#fff",
											borderRadius: 4,
											boxShadow:
												"0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
											marginBottom: i === booking_list.length - 1 ? 100 : 0,
										}}
									>
										<div
											style={{ width: "100%", height: 130, display: "flex" }}
										>
											<div
												style={{
													width: "25%",
													height: 130,
													display: "flex",
													justifyContent: "center",
													alignItems: "center",
												}}
											>
												{data.vehicle_type === "car" ? (
													<DriveEtaIcon style={{ color: "#0b27c4" }} />
												) : data.vehicle_type === "bike" ? (
													<DirectionsBikeOutlinedIcon />
												) : data.vehicle_type === "taxi" ? (
													<LocalTaxiOutlinedIcon />
												) : (
													<b
														style={{
															fontWeight: 500,
															color: "#039af4",
															fontSize: 17,
														}}
													>
														o
													</b>
												)}
											</div>
											<div
												style={{
													width: "75%",
													height: 100,
													flexDirection: "column",
												}}
											>
												<div
													style={{
														textAlign: "left",
														padding: 2,
														paddingTop: 7,
														color: "#222",
														fontFamily: "openbold",
														textTransform: "capitalize",
													}}
												>
													{data.vehicle_make_model}
												</div>
												<div
													style={{
														textAlign: "left",
														paddingTop: 2,
														color: "#6b6b6b",
														fontFamily: "open",
														fontSize: 12,
														textTransform: "uppercase",
													}}
												>
													<b
														style={{
															color: "#222",
															fontWeight: 600,
															fontSize: 10,
														}}
													>
														Reg no.{" "}
													</b>
													&nbsp;
													{data.vehicle_reg_no}
												</div>
												<div
													style={{
														textAlign: "left",
														paddingTop: 2,
														color: "#6b6b6b",
														fontFamily: "open",
														fontSize: 12,
													}}
												>
													<b
														style={{
															color: "#222",
															fontWeight: 600,
															fontSize: 10,
														}}
													>
														Booking Date{" "}
													</b>{" "}
													&nbsp;
													{moment(data.start_time).format("ll")}
												</div>

												<div
													style={{
														textAlign: "left",
														paddingTop: 2,
														color: "#6b6b6b",
														fontFamily: "open",
														fontSize: 12,
													}}
												>
													<b
														style={{
															color: "#222",
															fontWeight: 600,
															fontSize: 10,
														}}
													>
														Booking Time{" "}
													</b>{" "}
													&nbsp;
													{moment(data.start_time).format("LT")}
												</div>

												<div
													style={{
														textAlign: "left",
														paddingTop: 2,
														color: "#6b6b6b",
														fontFamily: "open",
														fontSize: 12,
													}}
												>
													<b
														style={{
															color: "#222",
															fontWeight: 600,
															fontSize: 10,
														}}
													>
														Otp{" "}
													</b>{" "}
													&nbsp;
													{data.otp}
												</div>
											</div>
										</div>
										<div
											style={{
												width: "100%",
												height: 50,
												display: "flex",
												justifyContent: "flex-end",
												alignItems: "center",
												border: "1px solid #eee",
												borderLeftColor: "transparent",
												borderRightColor: "transparent",
												borderBottomColor: "transparent",
											}}
										>
											<div
												style={{
													width: "60%",
													height: 50,
													display: "flex",
													justifyContent: "space-around",
													alignItems: "center",
													overflow: "hidden",
												}}
											>
												<a
													href={`https://nkda.sp.distronix.in/payu/${data.booking_id}`}
												>
													<Button
														type='dashed'
														shape='round'
														icon={
															<CreditCardFill style={{ color: "#673AB7" }} />
														}
														style={{ fontWeight: 600 }}
													>
														&nbsp;Pay
													</Button>
												</a>

												<Modal item={data} name='details' />
											</div>
										</div>
									</div>
								</Col>
							))
						) : (
							<Card
								style={{ width: "100%", marginTop: 16 }}
								actions={[
									<EditOutlined key='edit' />,
									<EllipsisOutlined key='ellipsis' />,
								]}
							>
								<Skeleton
									loading={booking_list.length === 0 ? true : false}
									avatar
									active
								>
									<Meta
										avatar={
											<Avatar src='https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png' />
										}
										title='Card title'
										description='This is the description'
									/>
								</Skeleton>
							</Card>
						)}
					</Row>
				</TabPane>

				<TabPane tab='Past' key='2' style={{ width: "100%" }}>
					<Row justify='center'>
						{pastLength > 0 ? (
							booking_list.length > 0 ? (
								booking_list.map(
									(data, i) =>
										data.end_time !== null && (
											<Col
												className='gutter-row'
												xs={22}
												lg={5}
												style={{ margin: 5 }}
											>
												<div
													style={{
														width: "100%",
														height: 180,
														flexDirection: "column",

														background: "#fff",
														borderRadius: 4,
														boxShadow:
															"0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
														marginBottom:
															i === booking_list.length - 1 ? 100 : 0,
													}}
												>
													<div
														style={{
															width: "100%",
															height: 130,
															display: "flex",
														}}
													>
														<div
															style={{
																width: "25%",
																height: 130,
																display: "flex",
																justifyContent: "center",
																alignItems: "center",
															}}
														>
															{data.vehicle_type === "car" ? (
																<DriveEtaIcon style={{ color: "#0b27c4" }} />
															) : data.vehicle_type === "bike" ? (
																<DirectionsBikeOutlinedIcon />
															) : data.vehicle_type === "taxi" ? (
																<LocalTaxiOutlinedIcon />
															) : (
																<b
																	style={{
																		fontWeight: 500,
																		color: "#039af4",
																		fontSize: 17,
																	}}
																>
																	o
																</b>
															)}
														</div>
														<div
															style={{
																width: "75%",
																height: 100,
																flexDirection: "column",
															}}
														>
															<div
																style={{
																	textAlign: "left",
																	padding: 2,
																	paddingTop: 7,
																	color: "#222",
																	fontFamily: "openbold",
																	textTransform: "capitalize",
																}}
															>
																{data.vehicle_make_model}
															</div>
															<div
																style={{
																	textAlign: "left",
																	paddingTop: 2,
																	color: "#6b6b6b",
																	fontFamily: "open",
																	fontSize: 12,
																	textTransform: "uppercase",
																}}
															>
																<b
																	style={{
																		color: "#222",
																		fontWeight: 600,
																		fontSize: 10,
																	}}
																>
																	Reg no.{" "}
																</b>
																&nbsp;
																{data.vehicle_reg_no}
															</div>
															<div
																style={{
																	textAlign: "left",
																	paddingTop: 2,
																	color: "#6b6b6b",
																	fontFamily: "open",
																	fontSize: 12,
																}}
															>
																<b
																	style={{
																		color: "#222",
																		fontWeight: 600,
																		fontSize: 10,
																	}}
																>
																	Booking Date{" "}
																</b>{" "}
																&nbsp;
																{moment(data.start_time).format("ll")}
															</div>

															<div
																style={{
																	textAlign: "left",
																	paddingTop: 2,
																	color: "#6b6b6b",
																	fontFamily: "open",
																	fontSize: 12,
																}}
															>
																<b
																	style={{
																		color: "#222",
																		fontWeight: 600,
																		fontSize: 10,
																	}}
																>
																	Booking Time{" "}
																</b>{" "}
																&nbsp;
																{moment(data.start_time).format("LT")}
															</div>

															<div
																style={{
																	textAlign: "left",
																	paddingTop: 2,
																	color: "#6b6b6b",
																	fontFamily: "open",
																	fontSize: 12,
																}}
															>
																<b
																	style={{
																		color: "#222",
																		fontWeight: 600,
																		fontSize: 10,
																	}}
																>
																	Amount{" "}
																</b>{" "}
																&nbsp; ₹ {data.amount}
															</div>
														</div>
													</div>
													<div
														style={{
															width: "100%",
															height: 50,
															display: "flex",
															justifyContent: "flex-end",
															alignItems: "center",
															border: "1px solid #eee",
															borderLeftColor: "transparent",
															borderRightColor: "transparent",
															borderBottomColor: "transparent",
														}}
													>
														<div
															style={{
																width: "30%",
																height: 50,
																display: "flex",
																justifyContent: "space-around",
																alignItems: "center",
															}}
														>
															<Modal item={data} name='receipt' />
														</div>
													</div>
												</div>
											</Col>
										)
								)
							) : (
								<Card
									style={{ width: "100%", marginTop: 16 }}
									actions={[
										<EditOutlined key='edit' />,
										<EllipsisOutlined key='ellipsis' />,
									]}
								>
									<Skeleton
										loading={booking_list.length === 0 ? true : false}
										avatar
										active
									>
										<Meta
											avatar={
												<Avatar src='https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png' />
											}
											title='Card title'
											description='This is the description'
										/>
									</Skeleton>
								</Card>
							)
						) : (
							<b
								style={{
									fontFamily: "openbold",
									fontSize: 20,
									color: "#6b6b6b",
									marginTop: 50,
								}}
							>
								{" "}
								No Past booking found!!!
							</b>
						)}
					</Row>
				</TabPane>
			</Tabs>
		</>
	);
}
