import React, { useState } from "react";
import { Map, TileLayer, Popup, Marker, CircleMarker } from "react-leaflet";
import { Badge, Form, Button as But, TimePicker, Select, message } from "antd";
import { CarTwoTone } from "@ant-design/icons";
import { getBookingList } from "../redux/actions";
import { useDispatch, useSelector } from "react-redux";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";
import axios from "axios";

const useStyles = makeStyles(theme => ({
	root: {
		width: "100%",
	},
	heading: {
		fontSize: theme.typography.pxToRem(15),
	},
	secondaryHeading: {
		fontSize: theme.typography.pxToRem(14),
		color: "#0b27c4",
		marginTop: 5,
	},
	icon: {
		verticalAlign: "bottom",
		height: 20,
		width: 20,
	},
	details: {
		alignItems: "center",
	},
	column: {
		flexBasis: "33.33%",
	},
	helper: {
		borderLeft: `2px solid ${theme.palette.divider}`,
		padding: theme.spacing(1, 2),
	},
	link: {
		color: theme.palette.primary.main,
		textDecoration: "none",
		"&:hover": {
			textDecoration: "underline",
		},
	},
}));

export default function Home() {
	const [form] = Form.useForm();
	const [isSubmitting, setIsSubmitting] = useState(false);
	const [expand, setExpand] = useState(false);

	const [vehicle_id, setVehicle_Id] = React.useState("");
	const [valid, setValid] = React.useState(true);

	const dispatch = useDispatch();
	const { user } = useSelector(state => state.user);
	const { details, vehicle_list, booking_list } = useSelector(
		state => state.data
	);

	const classes = useStyles();

	function onChange(time, timeString) {
		console.log(timeString);
		console.log(
			moment().format("LT"),
			"after adding",
			moment().add(15, "minutes").format("LT")
		);
		if (
			moment(time).format("LT") >= moment().format("LT") &&
			moment(time).format("LT") <= moment().add(15, "minutes").format("LT")
		) {
			console.log("valid time");
			setValid(true);
		} else {
			setValid(false);

			message.warn(
				`Time must be in this range ${moment().format("LT")} - ${moment()
					.add(15, "minutes")
					.format("LT")}`,
				4
			);
		}
	}

	const { Option } = Select;

	function onChange2(value) {
		console.log(`selected ${value}`);
		setVehicle_Id(value);
	}

	function onBlur() {
		console.log("blur");
	}

	function onFocus() {
		console.log("focus");
	}

	function onSearch(val) {
		console.log("search:", val);
	}

	const addBooking = () => {
		const bookingData = {
			user_id: user.user_id,
			phone: user.phone,
			vehicle_id: vehicle_id,
		};
		setIsSubmitting(true);

		if (vehicle_id !== "") {
			if (valid) {
				const count = booking_list.filter(veh => veh.vehicle_id === vehicle_id)
					.length;
				if (count > 0) {
					message.error("vehicle already in ongoing status");
					setIsSubmitting(false);
				} else {
					axios
						.post("/make_new_booking", bookingData)
						.then(res => {
							setIsSubmitting(false);
							message.success("Bookig added successfully");
							setExpand(false);

							dispatch(
								getBookingList({
									user_id: user.user_id,
									page_no: 1,
									item_per_page: 100,
								})
							);
						})
						.catch(err => {
							setIsSubmitting(false);
						});
				}
			} else {
				setIsSubmitting(false);
				message.warn(
					`Time must be in this range ${moment().format("LT")} - ${moment()
						.add(15, "minutes")
						.format("LT")}`,
					4
				);
			}
		} else {
			setIsSubmitting(false);
			message.warn(`select vehicle registration no`, 4);
		}
	};

	return (
		<div
			style={{
				width: "100%",
				height: "-webkit-fill-available",
				position: "fixed",
			}}
		>
			{details.length > 0 && (
				<Map
					center={[details[1].latitude, details[1].longitude]}
					zoom={42}
					style={{ zIndex: -1, position: "absolute" }}
				>
					<TileLayer
						attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
						url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
					/>
					<CircleMarker
						center={[details[1].latitude, details[1].longitude]}
						color='red'
						radius={20}
					>
						<Popup>NewTown Kolkata</Popup>
					</CircleMarker>
					<Marker position={[details[1].latitude, details[1].longitude]}>
						<Popup>
							{details[1].name} <br />
							{details[1].address}
						</Popup>
					</Marker>
				</Map>
			)}
			<div
				style={{
					width: 300,
					background: "#fff",
					height: "fit-content",
					position: "absolute",
					zIndex: 9,
					transform: "translate(-50%,0)",
					left: "50%",
					bottom: 75,
					boxShadow:
						"0 0 0 1px rgba(255,255,255,.1), 0 2px 4px 0 rgba(14,30,37,.12)",
					borderRadius: 5,
					overflow: "hidden",
				}}
			>
				<div
					style={{
						width: "100%",
						height: 80,
						display: "flex",
						justifyContent: "center",
						alignItems: "center",
						fontSize: 20,
					}}
				>
					<b
						style={{
							fontWeight: 500,
							width: "80%",
							fontSize: 17,
							color: "#222",
							marginLeft: "20%",
							lineHeight: "initial",
							fontFamily: "open",
						}}
					>
						{details.length > 0 && details[1].name}
					</b>
				</div>
				<div
					style={{
						display: "flex",
						width: "100%",
						height: 80,
					}}
				>
					<div
						style={{
							width: "20%",
							height: "100%",
							display: "flex",
							justifyContent: "center",
							alignItems: "center",
						}}
					>
						<Badge
							count={
								details[1].available_bay_count && details[1].available_bay_count
							}
						>
							<CarTwoTone style={{ fontSize: 35 }} twoToneColor='#0b27c4' />{" "}
						</Badge>
					</div>
					<div
						style={{
							width: "75%",
							height: "100%",
							flexDirection: "column",
						}}
					>
						<p
							style={{ margin: 0, marginTop: 4, fontWeight: 500, fontSize: 12 }}
						>
							Available Bays:{" "}
							{details[1].available_bay_count && details[1].available_bay_count}
						</p>
						<b style={{ fontSize: 11, color: "#6b6b6b", width: "90%" }}>
							Address:{" "}
							<b style={{ fontSize: 10, fontWeight: 400 }}>
								{details[1].name}
								{details[1].address}
							</b>
						</b>
					</div>
				</div>
				<Accordion expanded={expand}>
					<AccordionSummary
						expandIcon={<ExpandMoreIcon onClick={() => setExpand(!expand)} />}
						aria-controls='panel1c-content'
						id='panel1c-header'
					>
						<div className={classes.column}>
							<a
								href={`https://www.google.com/maps/search/?api=1&query=${details[1].latitude},${details[1].longitude}`}
							>
								<But
									type='dashed'
									shape='round'
									icon={
										<img
											src={require("../images/gm.png")}
											style={{
												width: 20,
												height: 20,
												marginTop: -10,
												marginLeft: -5,
											}}
										/>
									}
									size={100}
								>
									Navigate
								</But>
							</a>{" "}
						</div>
						<div
							className={classes.column}
							style={{ flexBasis: "50%", position: "absolute", right: 44 }}
						>
							<Typography
								className={classes.secondaryHeading}
								onClick={() => setExpand(!expand)}
							>
								BOOK Now
							</Typography>
						</div>
					</AccordionSummary>
					<AccordionDetails className={classes.details}>
						<Form form={form} layout='vertical' style={{ width: "100%" }}>
							<Form.Item label='Select Booking Time' required>
								<TimePicker
									use12Hours
									onChange={onChange}
									style={{ width: "100%" }}
								/>
							</Form.Item>
							<Form.Item label='Select Vehicle'>
								<Select
									showSearch
									style={{
										width: "100%",
										textAlign: "left",
									}}
									placeholder='Select Vehicle'
									optionFilterProp='children'
									onChange={onChange2}
									onFocus={onFocus}
									onBlur={onBlur}
									onSearch={onSearch}
									filterOption={(input, option) =>
										option.children
											.toLowerCase()
											.indexOf(input.toLowerCase()) >= 0
									}
								>
									{vehicle_list.length > 0
										? vehicle_list.map((data, i) => (
												<Option
													key={i}
													value={data.vehicle_id}
													style={{ textTransform: "uppercase" }}
												>
													{data.vehicle_reg_no}
												</Option>
										  ))
										: "no vehicle found"}
								</Select>
							</Form.Item>
						</Form>
					</AccordionDetails>
					<Divider />
					<AccordionActions>
						<Button
							size='small'
							style={{ fontFamily: "open" }}
							onClick={() => {
								setExpand(false);
							}}
						>
							Cancel
						</Button>
						<But
							loading={isSubmitting}
							type='primary'
							style={{
								background: "#0b27c4",
								fontFamily: "open",
								borderRadius: 50,
							}}
							onClick={addBooking}
						>
							Confirm
						</But>
					</AccordionActions>
				</Accordion>
			</div>
		</div>
	);
}
