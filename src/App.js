import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";

import Login from "./pages/Login";
import Main from "./pages/Main";
import ForgotPassword from "./pages/ForgotPassword";

import AuthRoute from "./util/AuthRoute";
import SecondRoute from "./util/SecondRoute";
import axios from "axios";
import { availableBays, getVehicleList, getBookingList } from "./redux/actions";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import "antd/dist/antd.css";

axios.defaults.baseURL = "https://nkda.sp.distronix.in:3100/citizen_app";

function App() {
	const { user } = useSelector(state => state.user);
	const dispatch = useDispatch();

	React.useEffect(() => {
		document.body.style.backgroundColor = "#f7f7f7";

		if (user.user_id) {
			dispatch(availableBays({ user_id: user.user_id }));
			dispatch(getVehicleList({ user_id: user.user_id }));
			dispatch(
				getBookingList({
					user_id: user.user_id,
					page_no: 1,
					item_per_page: 300,
				})
			);
		}
	}, [user]);
	return (
		<div className='App'>
			<Router>
				<Switch>
					<AuthRoute exact path='/' component={Main} />
					<SecondRoute exact path='/login' component={Login} />
					<SecondRoute
						exact
						path='/forgotPassword'
						component={ForgotPassword}
					/>
				</Switch>
			</Router>
		</div>
	);
}

export default App;
