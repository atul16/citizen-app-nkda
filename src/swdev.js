export default function swdev() {
	let swurl = `${process.env.PUBLIC_URL}/sw.js`;
	navigator.serviceWorker.register(swurl).then(res => {
		console.log(res);
	});
}
