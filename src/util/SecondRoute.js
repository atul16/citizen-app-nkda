import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

export default function AuthRoute({ component: Component, ...rest }) {
	const { loggedIn } = useSelector(state => state.user);
	console.log("from sr", loggedIn);
	return (
		<Route
			{...rest}
			render={props =>
				loggedIn ? <Redirect to='/' /> : <Component {...props} />
			}
		/>
	);
}
