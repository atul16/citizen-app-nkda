import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

function AuthRoute({ component: Component, ...rest }) {
	const { loggedIn } = useSelector(state => state.user);
	console.log("from ar", loggedIn);
	return (
		<Route
			{...rest}
			render={props =>
				!loggedIn ? <Redirect to='/login' /> : <Component {...props} />
			}
		/>
	);
}
export default React.memo(AuthRoute);
